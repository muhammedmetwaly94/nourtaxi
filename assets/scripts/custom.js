$(document).ready(function () {
  $('.custom_select').select2({
    dir: 'rtl'
  })
  $('.timepicker').timepicker({
    timeFormat: 'h:mm'
  })

  $('#users-all').change(function () {
    if ($(this).is(':checked')) {
      $('.checkbox_custom input').not(this).prop('checked', true)
    } else {
      $('.checkbox_custom input').not(this).prop('checked', false)
    }
  })
  $('[type=checkbox]').change(function () {
    if ($(this).attr('id') != 'users-all') {
      $('#users-all').prop('checked', false)
    }
  })

  $('.repeated_form button').click(function () {
    const firstEl = $(this).siblings('.form-row:first')
    const cloned = firstEl.clone()

    cloned.removeClass('d-none').insertAfter($(this).siblings('.form-row:last'))
    cloned.find('.bootstrap-tagsinput').remove()
    cloned.find('input').val('')
  })

  $('body').on('click', '.remove_el', function () {
    $(this).parents('.single_item').remove()
  })


  // datatable
  $('.dataTable').dataTable({
    "language": {
      "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
    },
  });

});
// notification toaster
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-center",
  "preventDuplicates": false,
  "onclick": null,
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}