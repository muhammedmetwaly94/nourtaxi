var options = {
    bar: {
        groupWidth: "90%"
    },
    fontSize: 12,
    fontName: 'Tajawal',
    vAxis: {
        gridlines: {
            count: 0
        },
        fontSize: 0,

        textColor: '#fff',
        baselineColor: '#fff',
        ticks: []
    },
    hAxis: {
        gridlines: {
            count: 0
        },
        fontSize: 0,
        format: '#,###%',
        textColor: '#fff',
        baselineColor: '#fff',
        ticks: []
    },
    legend: 'none',
    backgroundColor: {
        fill: 'transparent'
    },
};

google.charts.load("current", {
    packages: ['corechart']
});

google.charts.setOnLoadCallback(drawChart);


function drawChart() {
    document.querySelectorAll('.chart_shape').forEach((elm) => {
        var aa = [
            ["Element", elm.getAttribute('data-chart-label'), {
                role: "style"
            }],
            [elm.getAttribute('data-first-label'), parseInt(elm.getAttribute('data-done')), "color:  #48A240"],
            [elm.getAttribute('data-second-label'), parseInt(elm.getAttribute('data-remaining')), "color: #DC3912"]
        ];

        var data = google.visualization.arrayToDataTable(aa);
        var view = new google.visualization.DataView(data);

        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2
        ]);

        var chart = new google.visualization.ColumnChart(elm);
        chart.draw(view, options);
    })
}